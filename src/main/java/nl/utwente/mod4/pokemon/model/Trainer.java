package nl.utwente.mod4.pokemon.model;


public class Trainer extends NamedEntity {

    public String profileUrl;

    public Trainer() {
        super();
        profileUrl = null;
    }
}
